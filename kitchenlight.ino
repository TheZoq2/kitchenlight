#include "FastLED.h"

// How many leds in your strip?
#define NUM_LEDS 100

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806 define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 17
#define CLOCK_PIN 13

const int BUTTON_READ_PIN = 11;

// Define the array of leds
CRGB leds[NUM_LEDS];

void setup() {
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);

    pinMode(BUTTON_READ_PIN, INPUT_PULLDOWN);
    pinMode(10, OUTPUT);

    digitalWrite(10, HIGH);

    Serial.begin(115200);
}

bool led_state = false;
bool prev_button_state = false;


void loop() {
    if(digitalRead(BUTTON_READ_PIN) == true && prev_button_state == false) {
        prev_button_state = true;
        // Debounce
        delay(10);


    }
    else if(digitalRead(BUTTON_READ_PIN) == false) {
        if (prev_button_state == true) {
            led_state = !led_state;
            delay(10);
        }
        prev_button_state = false;
    }

    update_leds();
}

void update_leds() {
    for(int i = NUM_LEDS-10; i < NUM_LEDS; ++i) {
        leds[i] = led_state ? CRGB::Yellow : CRGB::Black;
    }
    FastLED.show();
}
